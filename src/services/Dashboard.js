import Api from "./Api";
import {
  formatWidgetData,
  formatTopUsers,
  formatRequest
} from "../utils/dashboard";

export default class Dashboard extends Api {
  getConversationsCount = query =>
    formatRequest(query, async params => {
      const data = (await super.get("conversations/count", params)).data;
      return formatWidgetData({ ...data, type: "conversations" });
    });

  getMessagesCount = query =>
    formatRequest(query, async params => {
      const data = (await super.get("messages/count", params)).data;
      return formatWidgetData({ ...data, type: "messages" });
    });

  getLoansCount = query =>
    formatRequest(query, async params => {
      const data = (await super.get("loans/count", params)).data;
      return formatWidgetData({ ...data, type: "loans" });
    });

  getUsersCount = query =>
    formatRequest(query, async params => {
      const data = (await super.get("users/count", params)).data;
      return formatWidgetData({ ...data, type: "users" });
    });

  getTopUsers = async (params = { period: "yesterday" }) => {
    const { users } = (await super.get("messages/top-users", params)).data;
    const actions = users.map(({ id }) => this.getUserAvatar(id));
    const images = await Promise.all(actions);
    return formatTopUsers({ users, images });
  };

  getUserAvatar = async id => {
    const { url } = (await super.get(`users/${id}/avatar`)).data;
    return { id, url };
  };
}
