import axios from "axios";
import querystring from "querystring";

export default class Api {
  auth(user) {
    const params = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      withCredentials: true
    };

    return axios.post("/login/proceed", querystring.stringify(user), params);
  }

  get(url, params = {}) {
    return axios.get(`/api/v1/${url}`, { params });
  }
}
