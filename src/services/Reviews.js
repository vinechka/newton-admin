import Api from "./Api";

export default class Reviews extends Api {
  get = params => super.get(`reviews`, params);
}
