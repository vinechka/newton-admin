import Api from "./Api";

export default class Publications extends Api {
  get = params => super.get(`publications`, params);
}
