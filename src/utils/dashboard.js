import moment from 'moment'
import 'moment/locale/ru'

const dateFormat = 'DDMMYYYY'

export const sortDays = (a, b) => {
  const dateA = moment(a, 'DD-MM-YYYY')
  const dateB = moment(b, 'DD-MM-YYYY')
  if (dateA > dateB) return 1
  if (dateA < dateB) return -1
  return 0
}

export const generateLink = ({ id, role }) => {
  const stagePrefix =
    window.location.pathname.indexOf('newton-admin') > -1 ? '/newton-admin' : ''
  if (role === 'ROLE_INVESTOR')
    return `${stagePrefix}/admin/party/investor/${id}`
  if (role === 'ROLE_BORROWER') return `${stagePrefix}/admin/party/loanee/${id}`
  if (role === 'ROLE_CONSULTANT') return `${stagePrefix}/admin/consultant/${id}`
}

export const formatDays = days =>
  days ? Object.entries(days).map(([date, count]) => ({ date, count })) : []

export const formatData = (data, props) => {
  const { daysReport } = data
  const currentCount = daysReport.length
    ? daysReport[daysReport.length - 1].count
    : 0
  const diff = currentCount ? (currentCount / daysReport[0].count) * 100 : 0
  const growing = diff < 100 ? false : true
  const diffResult = diff < 100 ? 100 - diff : diff - 100
  return { ...data, ...props, currentCount, diffResult, growing }
}

export const formatTypeData = type => {
  switch (type) {
    case 'messages':
      return { color: '#2FC25B', title: 'Сообщения', icon: 'message' }
    case 'loans':
      return { color: '#FACC14', title: 'Заявки', icon: 'pic-left' }
    case 'users':
      return { color: '#8543E0', title: 'Пользователи', icon: 'usergroup-add' }
    default:
      return { color: '#1890FF', title: 'Диалоги', icon: 'appstore' }
  }
}

export const formatWidgetData = data => {
  const daysReport = formatDays(data.daysReport).sort((a, b) =>
    sortDays(a.date, b.date)
  )

  return formatData({ ...data, daysReport }, formatTypeData(data.type))
}

export const formatTopUsers = ({ users, images }) => {
  const data = users.map(user => {
    const { url = '' } = images.find(({ id }) => id === user.id)
    const link = generateLink({ id: user.id, role: user.role })
    return { ...user, avatar: url, link }
  })

  return {
    users: data,
    type: 'top',
    title: 'Топ пользователей',
    icon: 'user',
    color: '#13C2C2'
  }
}

const subtractDate = (date, count, type = 'days') =>
  date.subtract(count, type).format(dateFormat)

// const generateParamsFromDate = ({ from, to }) => {
//   let previus = null
//   const current = {
//     from: from.format(dateFormat),
//     to: to.format(dateFormat)
//   }
//   const daysCount = to.dayOfYear() - from.dayOfYear() + 1

//   if (daysCount < 30) {
//     previus = {
//       from: subtractDate(from, daysCount),
//       to: subtractDate(to, daysCount)
//     }
//   } else {
//     previus = {
//       from: subtractDate(from, daysCount / 30, 'month'),
//       to: subtractDate(to, daysCount / 30, 'month')
//     }
//   }

//   return [previus, current]
// }

const generateParamsFromPeriod = period => {
  switch (period) {
    case 'today':
      return {
        from: subtractDate(moment(), 7, 'days')
      }

    case 'yesterday':
      return {
        from: subtractDate(moment(), 8, 'days'),
        to: subtractDate(moment(), 1, 'days')
      }

    default:
      return { period }
  }
}

export const formatRequest = ({ period, from, to }, getData) => {
  let params = null

  if (from) {
    params = { from: from.format(dateFormat), to: to.format(dateFormat) }
    // params = generateParamsFromDate({ from, to })
    // const actions = params.map(param => getData(param))
    // const [previus, current] = await Promise.all(actions)
    // return formatConversations({
    //   ...current.data,
    //   previus: previus.data,
    //   type: 'conversations'
    // })
  } else {
    params = generateParamsFromPeriod(period)
  }

  return getData(params)
}
