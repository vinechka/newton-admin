import React from 'react'
import { Form, Input, Select, Col, Row } from 'antd'

const { Option } = Select

const FilterForm = ({ onSubmit, onFieldChange, form }) => {
  const { getFieldDecorator } = form
  return (
    <Form onSubmit={onSubmit} style={{ marginBottom: 16 }}>
      <Row gutter={24}>
        <Col span={8}>
          {' '}
          <Form.Item style={{ marginBottom: 0 }}>
            {getFieldDecorator(`name`, {})(
              <Input
                placeholder="Имя"
                onChange={e =>
                  onFieldChange({ name: 'name', value: e.target.value })
                }
              />
            )}
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item style={{ marginBottom: 0 }}>
            {getFieldDecorator(`position`, {})(
              <Input
                placeholder="Должность"
                onChange={e =>
                  onFieldChange({ name: 'position', value: e.target.value })
                }
              />
            )}
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item style={{ marginBottom: 0 }}>
            {getFieldDecorator(`group`, {})(
              <Select
                placeholder="Выберите группу отзывов"
                onChange={e =>
                  onFieldChange({ name: 'group', value: e.target.value })
                }
              >
                <Option value="1">Заемщик ФЛ</Option>
                <Option value="2">Заемщик ЮЛ</Option>
                <Option value="3">Частные инвесторы</Option>
                <Option value="4">Инвесторы ЮЛ</Option>
                <Option value="5">Консультанты</Option>
                <Option value="6">Лента</Option>
              </Select>
            )}
          </Form.Item>
        </Col>
      </Row>
    </Form>
  )
}
export default Form.create({ name: 'reviews-filter' })(FilterForm)
