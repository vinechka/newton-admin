import React, { useState, useEffect } from "react";
import { Table, Avatar, Card } from "antd";
import { Link } from "react-router-dom";
import Publications from "../../services/Publications";
import InfiniteScroll from "react-infinite-scroller";

const { Column } = Table;
const PublicationsApi = new Publications();
const statuses = {
  draft: "Черновик",
  moderation: "На модерации",
  published: "Опубликована",
  rejected: "Отклонена",
};

export default ({ type }) => {
  const [params, setParams] = useState({ type, page: 0, pagesize: 20 });
  const [loading, setLoading] = useState(true);
  const [publications, setPublications] = useState([]);
  const [hasMore, setHasMore] = useState(true);

  useEffect(() => {
    const getReviews = async () => {
      setLoading(true);

      try {
        const { data } = (await PublicationsApi.get(params)).data;
        setPublications(items => items.concat(data));
        setLoading(false);
      } catch (e) {
        setHasMore(false);
        setLoading(false);
      }
    };

    getReviews();
    // return () => setPublications([])
  }, [params]);

  const handleInfiniteOnLoad = async () => {
    setLoading(true);
    setParams({ ...params, page: params.page + 1 });
  };

  return (
    <Card>
      <InfiniteScroll
        initialLoad={false}
        pageStart={0}
        loadMore={handleInfiniteOnLoad}
        hasMore={!loading && hasMore}
      >
        <Table
          bordered
          style={{ backgroundColor: "#fff" }}
          dataSource={publications}
          loading={loading}
          pagination={false}
        >
          <Column
            title="ID"
            dataIndex="id"
            asd
            key="id"
            sorter={(a, b) => a.id - b.id}
          />
          <Column
            title="Автор"
            dataIndex="author"
            key="author"
            render={author => (
              <span>
                <Avatar src={author.image} />
                <span style={{ paddingLeft: 8 }}>
                  {author.name.last || ""} {author.name.first || ""}
                </span>
              </span>
            )}
            sorter={(a, b) =>
              (a.author.name.last && a.author.name.last.length) -
              (b.author.name.last && b.author.name.last.length)
            }
          />
          <Column
            title="Заголовок"
            dataIndex="title"
            key="title"
            render={(title, record, index) => (
              <Link
                to={`/feed/article/${publications[index].id}`}
                style={{
                  display: "block",
                  whiteSpace: "nowrap",
                  maxWidth: 700,
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                }}
              >
                {title}
              </Link>
            )}
            sorter={(a, b) => a.title.length - b.title.length}
          />
          <Column
            title="Статус"
            dataIndex="status"
            key="status"
            render={status => <span>{statuses[status]}</span>}
            sorter={(a, b) => a.status.length - b.status.length}
          />
          {/* <Column
            title="Темы"
            dataIndex="tags"
            key="tags"
            render={tags => (
              <span>
                {tags.map(tag => (
                  <Tag color="blue" key={tag.id}>
                    {tag.name}
                  </Tag>
                ))}
              </span>
            )}
            sorter={(a, b) => a.tags.length - b.tags.length}
          /> */}
          <Column
            title="Просмотры"
            dataIndex="reactions.views"
            key="views"
            sorter={(a, b) => a.reactions.views - b.reactions.views}
          />
          <Column
            title="Лайки"
            dataIndex="reactions.likes"
            key="likes"
            sorter={(a, b) => a.reactions.likes - b.reactions.likes}
          />
          <Column
            title="Комментарии"
            dataIndex="reactions.comments"
            key="comments"
            sorter={(a, b) => a.reactions.comments - b.reactions.comments}
          />
          <Column
            title="Дата создания"
            dataIndex="createdAt"
            key="createdAt"
            render={date => <span>{new Date(date).toLocaleString()}</span>}
            sorter={(a, b) => a.createdAt - b.createdAt}
          />
        </Table>
      </InfiniteScroll>
    </Card>
  );
};
