import React from 'react'
import { Radio } from 'antd'

const DashboardFilter = ({ value, onChange }) => {
  return (
    <Radio.Group
      defaultValue={value}
      value={value}
      buttonStyle="solid"
      size="large"
      onChange={onChange}
    >
      <Radio.Button value="today">Сегодня</Radio.Button>
      <Radio.Button value="yesterday">Вчера</Radio.Button>
    </Radio.Group>
  )
}

export default DashboardFilter
