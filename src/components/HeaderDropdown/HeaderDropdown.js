import React from 'react'
import { Menu, Dropdown, Avatar, Icon } from 'antd'
import './HeaderDropdown.css'

const menu = (
  <Menu>
    <Menu.Item>
      <Icon type="user" />
      <span>Профиль</span>
    </Menu.Item>
    <Menu.Item>
      <Icon type="setting" />
      <span>Настройки</span>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item>
      <Icon type="logout" />
      <span>Выйти</span>
    </Menu.Item>
  </Menu>
)

export default function HeaderDropdown() {
  return (
    <Dropdown overlay={menu} className="header-dropdown">
      <span>
        <Avatar size="small" icon="user" />
        <span className="header-dropdown__caption">Александр</span>
      </span>
    </Dropdown>
  )
}
