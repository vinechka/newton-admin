import React from 'react'
import { Icon } from 'antd'

const WidgetItemAction = ({ onActionClick }) => {
  return (
    <span onClick={onActionClick}>
      <Icon type="select" />
      <span style={{ marginLeft: 8 }}>Подробнее</span>
    </span>
  )
}

export default WidgetItemAction
