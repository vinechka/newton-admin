import React from 'react'
import { Chart, Geom, Tooltip } from 'bizcharts'
const cols = { date: { range: [0, 1] }, count: { min: 0 } }

const WidgetItemChart = ({ daysReport, color }) => {
  return (
    <div style={{ margin: '0' }}>
      <Chart
        forceFit={true}
        padding={0}
        height={46}
        data={daysReport}
        scale={cols}
      >
        <Tooltip />
        <Geom
          type="area"
          position="date*count"
          shape="smooth"
          opacity={0.85}
          color={color}
        />
      </Chart>
    </div>
  )
}

export default WidgetItemChart
