import React from 'react'
import { Typography, Icon } from 'antd'
const { Title, Text } = Typography

const WidgetsItemCount = ({ currentCount, diffResult, growing }) => (
  <div style={{ display: 'flex', alignItems: 'flex-end' }}>
    <Title style={{ margin: 0, fontWeight: 400 }}>{currentCount}</Title>
    <Text
      style={{
        display: 'flex',
        alignItems: 'center',
        margin: '0 0 5px 8px',
        color: growing ? 'green' : 'red'
      }}
    >
      <span>{diffResult.toFixed(2)}%</span>
      <Icon
        type={growing ? 'caret-up' : 'caret-down'}
        style={{
          fontSize: 10,
          marginLeft: 4
        }}
      />
    </Text>
  </div>
)

export default WidgetsItemCount
