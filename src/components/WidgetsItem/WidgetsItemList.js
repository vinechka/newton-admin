import React from 'react'
import { List, Avatar } from 'antd'

const emptyImage =
  'https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png'

const WidgetsItemList = ({ users, loadList }) => (
  <List
    itemLayout="horizontal"
    dataSource={users}
    loading={loadList}
    renderItem={({ id, name, count, avatar, link }) => (
      <List.Item key={id} extra={<p>{count}</p>}>
        <List.Item.Meta
          avatar={<Avatar src={avatar || emptyImage} />}
          title={<a href={link}>{name || 'Заемщик'}</a>}
        />
      </List.Item>
    )}
  />
)

export default WidgetsItemList
