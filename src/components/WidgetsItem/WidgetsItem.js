import React, { useState, useEffect } from 'react'
import { Card, Icon } from 'antd'
import WidgetsItemAction from './WidgetsItemAction'

const WidgetsItem = ({
  auth,
  getData,
  tabList,
  activeTab,
  activePeriod,
  onTabChange,
  onAdditionalSelected,
  children
}) => {
  const [data, setData] = useState({})
  const [loading, setLoading] = useState(true)
  const [loadList, setLoadList] = useState(true)

  useEffect(() => setLoading(true), [activePeriod])
  useEffect(() => setLoadList(true), [activeTab])
  useEffect(() => {
    if (auth) {
      getData().then(res => {
        setData(res)
        setLoading(false)
        setLoadList(false)
      })
    }
  }, [auth, getData])

  const actions = onAdditionalSelected
    ? [<WidgetsItemAction onActionClick={() => onAdditionalSelected(data)} />]
    : null

  if (!loading) {
    const { title, color, icon } = data
    return (
      <Card
        title={!tabList ? title : null}
        extra={
          !tabList ? <Icon style={{ color, fontSize: 18 }} type={icon} /> : null
        }
        actions={actions}
        loading={loading}
        tabList={tabList}
        activeTabKey={activeTab}
        onTabChange={onTabChange}
        style={{ marginTop: 24 }}
      >
        <div style={{ margin: '-10px 0' }}>
          {React.Children.map(children, child =>
            React.cloneElement(child, { ...data, loadList })
          )}
        </div>
      </Card>
    )
  } else {
    return <Card loading={loading} style={{ marginTop: 24 }} />
  }
}

export default WidgetsItem
