import React, { useState } from 'react'
import { Row, Col } from 'antd'

const DashboardWidgets = ({ children }) => {
  const [span] = useState(24)
  const [count] = useState(React.Children.count(children))

  return (
    <Row type="flex" gutter={24}>
      {React.Children.map(children, child => {
        const col = child.props.span || 0
        return <Col span={col || span / count}>{child}</Col>
      })}
    </Row>
  )
}

export default DashboardWidgets
