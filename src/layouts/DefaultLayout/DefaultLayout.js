import React, { useState } from "react";
import { Layout, Icon, Menu } from "antd";
import { Link, withRouter } from "react-router-dom";
import HeaderDropdown from "../../components/HeaderDropdown";
import "./DefaultLayout.css";
import logo from "../../logo.svg";

const { SubMenu } = Menu;
const { Sider, Content, Header } = Layout;
const location = window.location;

const DefaultLayout = ({ children }) => {
  const [collapsed, setCollapsed] = useState(false);
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo">
          <img src={logo} alt="Newton Admin" />
        </div>
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={["/"]}
          defaultOpenKeys={[location.pathname.includes("feed") ? "sub1" : ""]}
          selectedKeys={[location.pathname]}
        >
          <Menu.Item key="/">
            <Link to="/">
              <Icon type="dashboard" />
              <span>Дешборд</span>
            </Link>
          </Menu.Item>
          <SubMenu
            key="sub1"
            title={
              <span>
                <Icon type="layout" />
                <span>Лента</span>
              </span>
            }
          >
            <Menu.Item key="/feed/articles">
              <Link to="/feed/articles">Статьи</Link>
            </Menu.Item>
            <Menu.Item key="/feed/posts">
              <Link to="/feed/posts">Посты</Link>
            </Menu.Item>
            <Menu.Item key="3">Теги</Menu.Item>
            <Menu.Item key="4">Комментарии</Menu.Item>
            <Menu.Item key="5">Пользователи</Menu.Item>
          </SubMenu>
          <Menu.Item key="/reviews">
            <Link to="/reviews">
              <Icon type="copy" />
              <span>Отзывы</span>
            </Link>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header
          style={{
            background: "#fff",
            padding: "0 12px 0 0",
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <Icon
            className="trigger"
            type={collapsed ? "menu-unfold" : "menu-fold"}
            onClick={() => setCollapsed(!collapsed)}
          />
          <HeaderDropdown />
        </Header>
        <Content
          style={{
            padding: 24,
            minHeight: 280
          }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
};

export default withRouter(DefaultLayout);
