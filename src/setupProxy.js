const proxy = require("http-proxy-middleware");
const target = "http://admin.newton.finance/admin";
// const target = "http://stage.admin.newton.finance/admin";

module.exports = function(app) {
  app.use(
    proxy("/api", {
      target,
      changeOrigin: true
    })
  );

  app.use(
    proxy("/login", {
      target,
      changeOrigin: true
    })
  );

  app.use(
    proxy("/dashboard", {
      target,
      changeOrigin: true
    })
  );

  app.use(
    proxy("/media", {
      target: "http://admin.newton.finance",
      changeOrigin: true
    })
  );
};
