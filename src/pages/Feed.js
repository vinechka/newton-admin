import React, { useState, useEffect } from "react";
import DefaultLayout from "../layouts/DefaultLayout";
import { Typography, Table, Avatar, Tag } from "antd";
import Publications from "../services/Publications";
const { Title } = Typography;

const { Column } = Table;
const PublicationssApi = new Publications();
const statuses = {
  draft: "Черновик",
  moderation: "На модерации",
  published: "Опубликована",
  rejected: "Отклонена"
};
// const columns = [
//   {
//     title: "Заголовок",
//     dataIndex: "title",
//     sorter: (a, b) => a.name.length - b.name.length
//   },
//   {
//     title: "Статус",
//     dataIndex: "status",
//     sorter: (a, b) => a.company.length - b.company.length
//   },
//   {
//     title: "Дата создания",
//     dataIndex: "createdAt",
//     sorter: (a, b) => a.createdAt - b.createdAt
//   }
// ];

export default () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    const getPublications = async () => {
      const res = await PublicationssApi.get();
      setData(res);
    };
    getPublications();
    console.log(data);
  }, [data]);

  return (
    <DefaultLayout>
      <Title style={{ marginBottom: 24 }}>Статьи</Title>
      <Table style={{ backgroundColor: "#fff" }} dataSource={data}>
        <Column
          title="Автор"
          dataIndex="author"
          key="author"
          render={author => (
            <span>
              <Avatar src={author.image} />
              <span style={{ paddingLeft: 8 }}>
                {author.name.last || ""} {author.name.first || ""}
              </span>
            </span>
          )}
        />
        <Column title="Заголовок" dataIndex="title" key="title" />
        <Column
          title="Статус"
          dataIndex="status"
          key="status"
          render={status => <span>{statuses[status]}</span>}
        />
        <Column
          title="Темы"
          dataIndex="tags"
          key="tags"
          render={tags => (
            <span>
              {tags.map(tag => (
                <Tag color="blue" key={tag.id}>
                  {tag.name}
                </Tag>
              ))}
            </span>
          )}
        />
        <Column title="Дата создания" dataIndex="createdAt" key="createdAt" />
      </Table>
    </DefaultLayout>
  );
};
