import React, { useState } from "react";
import DashboardService from "../services/Dashboard";
import DefaultLayout from "../layouts/DefaultLayout";
import Widgets from "../components/Widgets";
import WidgetsItem, {
  WidgetsItemCount,
  WidgetsItemChart,
  WidgetsItemList
} from "../components/WidgetsItem";
import DashboardFilter from "../components/DashboardFilter";
import { Typography, Modal } from "antd";

const { Title } = Typography;

const tabList = [
  {
    key: "today",
    tab: "Сегодня"
  },
  {
    key: "yesterday",
    tab: "Вчера"
  },
  {
    key: "week",
    tab: "Неделя"
  },
  {
    key: "month",
    tab: "Месяц"
  },
  {
    key: "all",
    tab: "За все время"
  }
];

const {
  getConversationsCount,
  getMessagesCount,
  getLoansCount,
  getUsersCount,
  getTopUsers
} = new DashboardService();

const methods = [
  getConversationsCount,
  getMessagesCount,
  getLoansCount,
  getUsersCount
];

const Dashboard = ({ auth }) => {
  const [additional, setAdditional] = useState(null);
  const [tabKey, setTabKey] = useState("yesterday");
  const [period, setPeriod] = useState("yesterday");

  const changePeriod = e => {
    setPeriod(e.target.value);
    setTabKey(e.target.value);
  };

  return (
    <DefaultLayout>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <Title style={{ margin: 0 }}>Dashboard</Title>
        <DashboardFilter value={period} onChange={changePeriod} />
      </div>
      <Widgets>
        <Widgets span={18}>
          {methods.map(method => (
            <WidgetsItem
              span={8}
              key={method}
              auth={auth}
              activePeriod={period}
              getData={() => method({ period })}
              onAdditionalSelected={type => setAdditional(type)}
            >
              <WidgetsItemCount />
              <WidgetsItemChart />
            </WidgetsItem>
          ))}
        </Widgets>
        <WidgetsItem
          span={6}
          auth={auth}
          tabList={tabList}
          activeTab={tabKey}
          onTabChange={key => setTabKey(key)}
          getData={() => getTopUsers({ period: tabKey })}
        >
          <WidgetsItemList />
        </WidgetsItem>
      </Widgets>
      {additional ? (
        <Modal
          title={additional.title}
          visible={!!additional}
          width="60%"
          onOk={() => setAdditional("")}
          onCancel={() => setAdditional("")}
        >
          <p>Some contents...</p>
          <p>Some contents...</p>
          <p>Some contents...</p>
        </Modal>
      ) : null}
    </DefaultLayout>
  );
};

export default Dashboard;
