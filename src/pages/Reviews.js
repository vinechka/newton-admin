import React, { useState, useEffect } from 'react'
import DefaultLayout from '../layouts/DefaultLayout'
import { Typography, Table, Card } from 'antd'
import { Link } from 'react-router-dom'
import Reviews from '../services/Reviews'
import InfiniteScroll from 'react-infinite-scroller'
const { Title } = Typography

const ReviewsApi = new Reviews()

export default () => {
  const [params, setParams] = useState({ page: 0 })
  const [loading, setLoading] = useState(true)
  const [reviews, setReviews] = useState([])
  const [hasMore, setHasMore] = useState(true)

  // const handleFilterChange = async ({ name, value }) => {
  //   if (value) {
  //     setParams({ ...params, page: 0, [name]: value })
  //   } else {
  //     setParams({ ...params, page: 0, [name]: null })
  //   }
  // }

  useEffect(() => {
    const getReviews = async () => {
      setLoading(true)

      try {
        const { data } = (await ReviewsApi.get(params)).data
        setReviews(items => items.concat(data))
        setLoading(false)
      } catch (e) {
        setLoading(false)
        setHasMore(false)
      }
    }
    getReviews()

    // return () => setReviews([])
  }, [params])

  const handleInfiniteOnLoad = () => {
    setParams({ ...params, page: params.page + 1 })
  }

  const columns = [
    {
      title: 'Имя',
      dataIndex: 'name',
      sorter: (a, b) => a.name.length - b.name.length,
      render: (name, record) => <Link to={`/reviews/${record.id}`}>{name}</Link>
    },
    {
      title: 'Должность',
      dataIndex: 'position',
      sorter: (a, b) => a.position.length - b.position.length
    },
    {
      title: 'Группа',
      dataIndex: 'group',
      sorter: (a, b) => a.group.length - b.group.length
    },
    {
      title: 'Дата',
      dataIndex: 'createdAt',
      sorter: (a, b) => a.createdAt - b.createdAt,
      render: date => <span>{new Date(date).toLocaleString()}</span>
    }
  ]

  return (
    <DefaultLayout>
      <Title style={{ marginBottom: 24 }}>Отзывы</Title>
      <Card>
        {/* <ReviewsFilter onFieldChange={handleFilterChange} /> */}
        <InfiniteScroll
          initialLoad={false}
          pageStart={0}
          loadMore={handleInfiniteOnLoad}
          hasMore={!loading && hasMore}
        >
          <Table
            bordered
            style={{ backgroundColor: '#fff' }}
            columns={columns}
            dataSource={reviews}
            loading={loading}
            pagination={false}
            rowKey={record => record.id}
            expandedRowRender={record => (
              <p style={{ margin: '0 8px' }}>{record.reviewText}</p>
            )}
          />
        </InfiniteScroll>
      </Card>
    </DefaultLayout>
  )
}
