import React from "react";
import DefaultLayout from "../layouts/DefaultLayout";
import { Typography } from "antd";
import Publications from "../components/Publications";
const { Title } = Typography;

export default () => {
  return (
    <DefaultLayout>
      <Title style={{ marginBottom: 24 }}>Статьи</Title>
      <Publications type="post" />
    </DefaultLayout>
  );
};
