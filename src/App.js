import React, { useState, useEffect } from "react";
import "./App.css";
import Api from "./services/Api";
import { BrowserRouter as Router, Route } from "react-router-dom";

import { Dashboard, Feed, Articles, Posts, Auth, Reviews } from "./pages";

const App = () => {
  const [auth, setAuth] = useState(false);
  const api = new Api();

  useEffect(() => {
    api
      .auth({
        username: "ae@newton.finance",
        password: "Ermolaev1290145",

        // username: 'admin@newton.finance',
        // password: '%cR#D?WQK#h93xU3g+cA'
      })
      .then(res => console.log("success", res))
      .catch(e => {
        console.log("error", e);
        setAuth(true);
      });
  });

  return (
    <Router>
      <Route exact path="/" render={() => <Dashboard auth={auth} />} />
      <Route exact path="/feed" component={Feed} />
      <Route path="/feed/articles" component={Articles} />
      <Route path="/feed/posts" component={Posts} />
      <Route path="/auth" component={Auth} />
      <Route path="/reviews" component={Reviews} />
    </Router>
  );
};

export default App;
